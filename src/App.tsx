import React from 'react'
import Modal from './page/Modal/index'
import Test from './page/Test/index'

const App: React.FC = () => {
  return (
    <div>
      <h2>App</h2>
      <Modal/>
      <Test/>
    </div>
  );
};
export default App;