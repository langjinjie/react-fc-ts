import React,{useState} from 'react'
// import style from './style.module.css'
import { Modal, Button, WhiteSpace, WingBlank, Icon } from 'antd-mobile';
import './index.css'

const ModalFc: React.FC = () => {
  const [modal1,setModal1] = useState<boolean>(false)
  // const [modal2,setModal2] = useState<boolean>(false)
  const clickBtn = ()=>{
    console.log('我点击了~');
    alert('我点击了~')
  }
  const showModal = () => {
    setModal1(true)
  }
  const onClose = () => {
    setModal1(true)
  }
  return (
    <>
     <h2>Modal</h2>
     <Button className='button' onClick={clickBtn}>按钮</Button>

     <WingBlank>
        <Button onClick={showModal}>basic</Button>
        <WhiteSpace />
        <Modal
          visible={modal1}
          transparent
          maskClosable={false}
          onClose={onClose}
          title="Title"
        >
          <div style={{ height: 100, overflow: 'scroll' }}>
            <h2>感谢您，我们已收到您的反馈</h2>
            我们会在1~2个工作日内审核该客户标签信息的准确性，再次感谢您的反馈，祝您身体健康，工作顺利
          </div>
        </Modal>
      </WingBlank>
    </>
  );
};
export default ModalFc;